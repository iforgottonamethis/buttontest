//
//  AppDelegate.h
//  Buttons!
//
//  Created by Umar on 12/13/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

