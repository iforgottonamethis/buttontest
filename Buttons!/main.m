//
//  main.m
//  Buttons!
//
//  Created by Umar on 12/13/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
