//
//  ViewController.m
//  Buttons!
//
//  Created by Umar on 12/13/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize randomButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    //setting up button
    randomButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 150, 150)];
    randomButton.center = self.view.center;
    randomButton.backgroundColor = [UIColor greenColor];
    [randomButton setTitle:@"test" forState:UIControlStateNormal];
    randomButton.titleLabel.text = @"text";
    [self.view addSubview:randomButton];
    randomButton.layer.masksToBounds = YES;
//    randomButton.layer.cornerRadius = randomButton.frame.size.width /2;
    
    UISlider *cornerRadiusSlider = [[UISlider alloc]initWithFrame:CGRectMake(25, 45, self.view.frame.size.width- 50, 20)];
    cornerRadiusSlider.minimumValue = 0;
    cornerRadiusSlider.maximumValue = randomButton.frame.size.width / 2;
//    cornerRadiusSlider.value = randomButton.layer.cornerRadius;
    [cornerRadiusSlider setThumbTintColor:[UIColor redColor]];
    [cornerRadiusSlider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:cornerRadiusSlider];
    


    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)sliderChanged:(UISlider *)slider{
    randomButton.layer.cornerRadius = slider.value;
}

@end
